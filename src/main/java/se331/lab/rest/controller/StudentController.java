package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Controller
public class StudentController {
    List<Student> students;
    public StudentController(){
        this.students = new ArrayList<>();;
        this.students.add(Student.builder()
                .id(Long.valueOf(1))
                .studentId("SE-001")
                .name("Wennan Lin")
                .surname("The minister")
                .gpa(3.59)
                .image("http://34.223.226.142:8190/images/z2.jpg")
                .penAmount(15)
                .description("The great G ever!!!")
                .build());
        this.students.add(Student.builder()
                .id(Long.valueOf(2))
                .studentId("SE-002")
                .name("Nile")
                .surname("BNK48")
                .gpa(4.01)
                .image("http://34.223.226.142:8190/images/z1.jpg")
                .penAmount(15)
                .description("The great G ever!!!")
                .build());
        this.students.add(Student.builder()
                .id(Long.valueOf(3))
                .studentId("SE-001")
                .name("G")
                .surname("The minister")
                .gpa(3.59)
                .image("http://34.223.226.142:8190/images/z3.jpg")
                .penAmount(15)
                .description("The great G ever!!!")
                .build());
    }
    @GetMapping("/students")
    public ResponseEntity getAllStudent(){
        return ResponseEntity.ok(students);
    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")Long id){
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }
    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
        student.setId((long) this.students.size());
        this.students.add(student);
        return ResponseEntity.ok(student);
    }
}
